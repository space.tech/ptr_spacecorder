# PTR_Spacecorder



## Co to jest?

Urządzenie będące analogiem Arecordera pod względem wymiarów i funkcjonalności. Główne cechy to
- pomiar przyspieszeń, prędkości obrotowych, pola magnetycznego, temperatury oraz ciśnienia
- logowanie pomiarów na kartę microSD i/lub wbudowaną pamięć Flash
- automatyczna detekcja stanów lotu
- obsługa maksymalnie 3 zapalników z pomiarem ciąglości
- odpalanie drugiego stopnia rakiety
- wyzwalanie spadochronu na pułapie
- wyzwalanie spadochronu głównego na zadanej wysokości

## Co jest na płytce?

- mikrokontroler NXP (dawne Freescale) MKS20FN lub MKS22FN - wersje 128 lub 256kB
- pamieć Flash SPI od 16Mb do 1Gb
- IMU MPU9250 (max 16g, 2000 deg/s)
- akcelerometr 3D LIS331HH (max 24g)
- akcelerometr 3D H3LIS331 (max 400g)
- KX134-1211 - wersja 1.1
- akceletrometr 2D MMA6827 (120g) - wersja 1.0
- barometr MS5607
- zasilanie 3.6-8.4V (teoretycznie do 16V powinno być ok)

## Co zawiera repozytorium?

- [X] Schematy wszystkich dotychczasowych rewizji
- [ ] Nietestowany w locie kod

## Foto góry płytki (bez wlutowanego gniazdo microSD)

![TOP](https://gitlab.com/space.tech/ptr_spacecorder/-/raw/main/foto/TOP_noSD.jpg)

## Foto dołu płytki

![BOT](https://gitlab.com/space.tech/ptr_spacecorder/-/raw/main/foto/BOT.jpg)

## Foto po zamontowaniu do szkieletu Cansata

![TOP](https://gitlab.com/space.tech/ptr_spacecorder/-/raw/main/foto/TOP_cansat.jpg)
